<html>
<head>
<style type="text/css" media="screen">
body { background: #e7e7e7; font-family: Verdana, sans-serif; font-size: 11pt; }
#page { background: #ffffff; margin: 50px; border: 2px solid #c0c0c0; padding: 10px; }
#header { background: #4b6983; border: 2px solid #7590ae; padding: 10px; color: #ffffff; }
#header h1 { color: #ffffff; text-align: center; }
#header p { color: #ffffff; text-align: center; }
#header p#hyper { text-align:left; text-decoration: none; color: white;}
#header a { text-decoration: none; color: white;}
#body { padding: 10px; text-align:center; }
#colap { display:block; }
hr { border: 2px solid; color: #4b6983; box-shadow: 2px 2px 5px #888888; }
span.tt { font-family: monospace; }
span.bold { font-weight: bold; }
table { border: 2px solid #7590ae; text-align: center; margin: auto; border-collapse:collapse;}
tr, td { border: 1pt solid black; padding:4px 15px; }
#thead { background: #4b6983; color: white; }
</style>
</head>
<?php
include '__conf.php';
$SOCK = socket_create(AF_INET,SOCK_STREAM,SOL_TCP);
socket_connect($SOCK,$settings["IP"],$settings["PORT"]);

socket_write($SOCK,pack("a4LLLLLL","ALRM",202,0,$_GET["id"],0,0,0));

$header = unpack("LMAGIC/LCMD/LFLAGS/LSIZE",socket_read($SOCK,16));
$data = unpack("LEPOCH/SID/CTYPE/CACTION/CNAMESIZE",socket_read($SOCK,9));
$name = socket_read($SOCK,$data["NAMESIZE"]);

socket_close($SOCK);
?>
<body>
<div id="page">
<div id="header">
	<p id="hyper"><a href="alarm.php">[BACK]</a></p>
	<h1>Confirm</h1>
</div>
<div id="body">
<?php 
if ($_GET["com"] == "del")
{
	if ($_GET["confirm"] == 1)
	{
		$SOCK = socket_create(AF_INET,SOCK_STREAM,SOL_TCP);
		socket_connect($SOCK,$settings["IP"],$settings["PORT"]);
		socket_write($SOCK,pack("a4LLL","ALRM",204,0,$_GET["id"]));
		socket_close($SOCK);
		printf("<h3>Deleted.</h3>");
	}
	else
	{
		printf("<h3>Delete '%s'?</h3>",$name);
		printf("<a href='docommand.php?com=%s&id=%s&confirm=%s'>[ CONFIRM DELETE ]</a>",$_GET["com"],$_GET["id"],"1");
	}
}
?>
</div>
</body>
</html>
