<?php
date_default_timezone_set("Australia/Melbourne");

function gen_entry($entry)
{
	include "__conf.php";
	printf("<tr><td><p class='s%d'>%s</p></td><td><p class='s%d'>%s</p></td><td><p class='s%d'>%s</p></td><td><p class='s%d'>%s</p></td><td><a href='docommand.php?com=del&id=%s'><img src='Cross16px.png'/></a></td></tr>",$entry["STATE"],$entry["NAME"],$entry["STATE"],date(DATE_RFC1036,($entry["EPOCH"])),$entry["STATE"],$actionNames[$entry["ACTION"]],$entry["STATE"],$typeNames[$entry["TYPE"]],$entry["ID"]);
}

function gen_all_entries()
{
	include "__conf.php";
	$SOCK = socket_create(AF_INET,SOCK_STREAM,SOL_TCP);
	socket_connect($SOCK,$settings["IP"],$settings["PORT"]);

	socket_write($SOCK,pack("a4LLLLLL","ALRM",200,0,0,0,0,0));

	$header = unpack("LMAG/LCMD/LFLAGS/LSIZE/LCOUNT",socket_read($SOCK,20));
	$data = "";

	if ($header['MAG'] == 0x4D524C41)
	{
		if ($header['CMD'] == 201)
		{
			for ($i = 0; $i < $header["COUNT"]; $i++)
			{
				$entry = unpack("LEPOCH/SID/CTYPE/CACTION/CSTATE/CNAMESIZE",socket_read($SOCK,10));
				$name = socket_read($SOCK,$entry["NAMESIZE"]);
				$entry["NAME"] = $name;
				gen_entry($entry);
			}
		}
	}
	socket_close($SOCK);
}
?>

<html>
<head>
<style type="text/css" media="screen">
body { background: #e7e7e7; font-family: Verdana, sans-serif; font-size: 11pt; }
#page { background: #ffffff; margin: 50px; border: 2px solid #c0c0c0; padding: 10px; }
#header { background: #4b6983; border: 2px solid #7590ae; padding: 10px; color: #ffffff; }
#header h1 { color: #ffffff; text-align: center;}
#header p { color: #ffffff; text-align: center; }
#header p#hyper { text-align:left; text-decoration: none; color: white;}
#header a { text-decoration: none; color: white;}
#body { padding: 10px; text-align:center; }
#colap { display:block; }
hr { border: 2px solid; color: #4b6983; box-shadow: 2px 2px 5px #888888; }
span.tt { font-family: monospace; }
span.bold { font-weight: bold; }
table { border: 2px solid #7590ae; text-align: center; margin: auto; border-collapse:collapse;}
tr, td { border: 1pt solid black; padding:4px 15px; }
p.s1 { color: red; }
p.s2 { text-decoration: line-through; }
#thead { background: #4b6983; color: white; }
</style>
</head>
<body>
<div id="page">
<div id="header">
	<p id="hyper"><a href="settings.php">[SETTINGS]</a></p>
	<h1>Super Alarm Clock</h1>
	<p>Configuration Page</p>
</div>
<div id="body">
<h3>Update Alarm</h3>
<table>
	<tr id="thead">
		<td>Name</td>
		<td>Next Trigger</td>
		<td>Action</td>
		<td>Type</td>
		<td><a href="createalarm.php"><img src="Plus16px.png"></a></td>
	</tr>
	<?php gen_all_entries(); ?>
</table>
</body>
</html>
