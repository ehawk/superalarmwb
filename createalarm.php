<?php
date_default_timezone_set("Australia/Melbourne");

function gen_types($form)
{
	include '__conf.php';
	print "<select class='sfl' name='type' form='" . $form . "'>";
	foreach ($typeNames as $id => $name)
	{
		print "<option value=" . $id . ">" . $name . "</option>";
	}
	print "</select>";
}

function gen_actions($form)
{
	include '__conf.php';
	print "<select class='sfl' name='action' form='" . $form . "'>";
	foreach ($actionNames as $id => $name)
	{
		print "<option value=" . $id . ">" . $name . "</option>";
	}
	print "</select>";
}

function gen_months($form)
{
	include '__conf.php';
	print '<select class="sfl" name="month" form="'.$form.'">';
	for ($i = 1; $i <= 12; $i++)
	{
		print '<option value="'.$i.'"';
		if ($i == date("m"))
			print ' selected';
		print '>';
		switch($i)
		{
			case 1:
				print 'January';
				break;
			case 2:
				print 'February';
				break;
			case 3:
				print 'March';
				break;
			case 4:
				print 'April';
				break;
			case 5:
				print 'May';
				break;
			case 6:
				print 'June';
				break;
			case 7:
				print 'July';
				break;
			case 8:
				print 'August';
				break;
			case 9:
				print 'September';
				break;
			case 10:
				print 'October';
				break;
			case 11:
				print 'November';
				break;
			case 12:
				print 'December';
				break;
		}
		print '</option>';
			
	}
	print '</select>';
}

function gen_days($form)
{
	include '__conf.php';
	print "<select class='sfl' name='day' form='" . $form . "'>";
	for ($i = 1; $i <= 31; $i++)
	{
		print "<option value='" . $i . "'";
		if (date("d") == $i)
			print " selected";
		print ">" . $i . "</option>";
	}
	print "</select>";
}

function gen_sounds($form)
{
	include '__conf.php';
	print "<select class='sfl' name='spath' form='" . $form . "'>";
	$SOCK = socket_create(AF_INET,SOCK_STREAM,SOL_TCP);
	socket_connect($SOCK,$settings["IP"],$settings["PORT"]);
	socket_write($SOCK,pack("a4LLLLLL","ALRM",300,0,0,0,0,0));
	
	$header = unpack("LMAGIC/LCMD/LFLAGS/LSIZE/LCOUNT",socket_read($SOCK,20));
	print var_dump($header);
	for ($i = 0; $i < $header["COUNT"]; $i++)
	{
		$datasize = unpack("CSIZE",socket_read($SOCK,1));
		$data = socket_read($SOCK,$datasize["SIZE"]);
		print $data;
		print "<option value=" . $data . ">" . $data . "</option>";
	}
	print "</select>";
	socket_close($SOCK);
}

function get_hour($time)
{
	sscanf($time,"%i:%i%s",$h,$m,$a);
	if ($h > 0 && $h <= 24)
	{
		if ($a == "PM")
		{
			if ($h > 12)
				return -1;
			return $h + 12;
		}
		return $h;
	}
	return -1;
}

function get_minute($time)
{
	sscanf($time,"%i:%i%s",$h,$m,$a);
	if ($m >= 0 && $m < 60)
	{
		return $m;
	}
	return -1;
}

?>
<html>
<head>
<style type="text/css" media="screen">
body { background: #e7e7e7; font-family: Verdana, sans-serif; font-size: 11pt; }
#page { background: #ffffff; margin: 50px; border: 2px solid #c0c0c0; padding: 10px; }
#header { background: #4b6983; border: 2px solid #7590ae; padding: 10px; color: #ffffff; }
#header h1 { color: #ffffff; text-align: center; }
#header p { color: #ffffff; text-align: center; }
#header p#hyper { text-align:left; text-decoration: none; color: white;}
#header a { text-decoration: none; color: white;}
#body { padding: 10px; text-align:center; }
#colap { display:block; }
hr { border: 2px solid; color: #4b6983; box-shadow: 2px 2px 5px #888888; }
span.tt { font-family: monospace; }
span.bold { font-weight: bold; }
table { border: 0px solid #7590ae; text-align: left; margin: auto; border-collapse:collapse;}
tr, td { border: 0pt solid black; padding:2px 4px; }
.sfl { width:100%; }
.ltr { text-align: right; }
</style>
</head>
<body>
<div id="page">
<div id="header">
	<p id="hyper"><a href="alarm.php">[BACK]</a></p>
	<h1>Super Alarm Clock</h1>
	<p>New Alarm Setup</p>
</div>
<div id="body">
<h3>Update Alarm</h3>
<?php
if ($_GET["submit"] == "1")
{
	include '__conf.php';
	
	if (get_hour($_GET["time"]) == -1 || get_minute($_GET["time"]) == -1 || mktime(get_hour($_GET["time"]),get_minute($_GET["time"]),0,$_GET["month"],$_GET["day"],0) == false)
	{
		print "Bad Time Format!\n";
		var_dump($_GET);
		print get_hour($_GET["time"]);
		print get_minute($_GET["time"]);
	}
	else
	{
		if ($_GET["type"] == "100")
		{
			$SOCK = socket_create(AF_INET,SOCK_STREAM,SOL_TCP);
			socket_connect($SOCK,$settings["IP"],$settings["PORT"]);
			
			socket_write($SOCK,pack("a4LLLLCCCa".strlen($_GET["name"])."Ca".strlen($_GET["spath"]),"ALRM",400,0,strlen($_GET["name"])+8+strlen($_GET["spath"]),mktime(get_hour($_GET["time"]),get_minute($_GET["time"]),0,$_GET["month"],$_GET["day"]),$_GET["type"],$_GET["action"],strlen($_GET["name"]),$_GET["name"],strlen($_GET["spath"]),$_GET["spath"]));
			socket_close($SOCK);
		}
		else if ($_GET["type"] == "101")
		{
			$SOCK = socket_create(AF_INET,SOCK_STREAM,SOL_TCP);
			socket_connect($SOCK,$settings["IP"],$settings["PORT"]);
			
			socket_write($SOCK,pack("a4LLLLCCCa".strlen($_GET["name"])."Ca".strlen($_GET["spath"])."L","ALRM",400,0,strlen($_GET["name"])+8+strlen($_GET["spath"]),mktime(get_hour($_GET["time"]),get_minute($_GET["time"]),0,$_GET["month"],$_GET["day"]),$_GET["type"],$_GET["action"],strlen($_GET["name"]),$_GET["name"],strlen($_GET["spath"]),$_GET["spath"],$_GET["repeat"]));
			socket_close($SOCK);
		}
		print $_GET["name"] . ' added.';
	}
}
else
{
	if ($_GET["action"] == "100")
	{
		if ($_GET["type"] == "100")
		{
			print '
			<form id="sact" method="get">
			<table><tr><td>Sound Path:</td><td>';
			gen_sounds("sact");
			print '</td></tr></table>                           
			<input type="hidden" name="type" value="' . $_GET["type"] . '">
			<input type="hidden" name="name" value="' . $_GET["name"] . '">
			<input type="hidden" name="action" value="' . $_GET["action"] . '">
			<input type="hidden" name="day" value="' . $_GET["day"] . '">
			<input type="hidden" name="month" value="' . $_GET["month"] . '">
			<input type="hidden" name="time" value="' . $_GET["time"] . '">
			<input type="hidden" name="submit" value="1">
			<input type="submit" value="Add Alarm">
			</form>
			';
		}
		else if ($_GET["type"] == "101")
		{
			print '
			<form id="sact" method="get">
			<table><tr><td>Sound Path:</td><td>';
			gen_sounds("sact");
			print '</td></tr></table>                           
			<input type="hidden" name="type" value="' . $_GET["type"] . '">
			<input type="hidden" name="name" value="' . $_GET["name"] . '">
			<input type="hidden" name="action" value="' . $_GET["action"] . '">
			<input type="hidden" name="day" value="' . $_GET["day"] . '">
			<input type="hidden" name="month" value="' . $_GET["month"] . '">
			<input type="hidden" name="time" value="' . $_GET["time"] . '">
			<input type="hidden" name="repeat" value="' . $_GET["repeat"] . '">
			<input type="hidden" name="submit" value="1">
			<input type="submit" value="Add Alarm">
			</form>
			';
		}
	}
	else
	{
		if ($_GET["type"] == "100")
		{
		print '
			<form id="date" method="get">
				<table>
				<tr><td>Day:</td>
				<td>';
				gen_days("date");
				print '
				</td></tr>
				<tr><td>Month:</td>
				<td>';
				gen_months("date");
				print '</td></tr>
				<tr><td>Time:</td><td><input type="time" name="time"></td></tr><tr><td>Action:</td><td>';
				gen_actions("date");
				print '
				</td></tr></table><input type="submit" value="Next">
				<input type="hidden" name="type" value="' . $_GET["type"] . '">
				<input type="hidden" name="name" value="' . $_GET["name"] . '">
			</form>';
		}
		else if ($_GET["type"] == "101")
		{
			print '
			<form id="date" method="get">
				<table>
				<tr><td>Start Day:</td>
				<td>';
				gen_days("date");
				print '
				</td></tr>
				<tr><td>Start Month:</td>
				<td><select class="sfl" name="month" form="date">
					<option value="1">January</option>
					<option value="2">February</option>
					<option value="3">March</option>
					<option value="4">April</option>
					<option value="5">May</option>
					<option value="6">June</option>
					<option value="7">July</option>
					<option value="8">August</option>
					<option value="9">September</option>
					<option value="10">October</option>
					<option value="11">November</option>
					<option value="12">December</option>
				</select></td></tr>
				<tr><td>Start Time:</td><td><input type="time" name="time"></td></tr><tr><td>Action:</td><td>';
				gen_actions("date");
				print '
				</td></tr><tr><td>Repeat Every (sec):</td>
				<td><input type="text" name="repeat"></td>
				</tr></table><input type="submit" value="Next">
				<input type="hidden" name="type" value="' . $_GET["type"] . '">
				<input type="hidden" name="name" value="' . $_GET["name"] . '">
			</form>';
		}
		else
		{
			print '
				<table>
				<form id="basic" method="get">
				<tr><td class="ltr">Alarm Name:</td><td><input name="name"></td></tr>
				<tr><td class="ltr">Type:</td><td>
				';
			gen_types("basic");
			print '
				</td></table>
				<input type="submit" value="Next">
				</form>
				';
		}
	}
}
?>
</div>
</div>
</body>
</html>
